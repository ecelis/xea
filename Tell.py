"""  XE logging facilities.

Licensed to you under one or more contributor license agreements.
See the NOTICE file distributed with this work for additional
information regarding copyright ownership.  Ernesto Angel Celis de la
Fuente licenses this file to you under the Apache License, Version 2.0
(the "License"); you may not use this file except in compliance with the
License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License. """

import logging

""" When true XE is becomes very verbose printing every log message to
stdout, regardless of logging level. """
verbose = True
# TODO implement verbosity

def info(message):
    logging.info(message)
    print(message)

def warn(message):
    logging.warn(message)
    print(message)

def error(message):
    logging.error(message)
    print(message)
