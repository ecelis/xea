""" XE Database object facilities 

Licensed to you under one or more contributor license agreements.
See the NOTICE file distributed with this work for additional
information regarding copyright ownership.  Ernesto Angel Celis de la
Fuente licenses this file to you under the Apache License, Version 2.0
(the "License"); you may not use this file except in compliance with the
License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.  """ 

import os
import shutil
import Tell

tell = Tell
_name = None
_absolutepath = None

def create(database):
    """ Creates a new database if does not already exists. """
    if(os.path.exists(database)):
        tell.error(database + ' already exists!')
    else:
        os.system('mkdir ' + database)
        tell.info(database + ' created!')


def drop(database):
    """ Removes a database from current collection. WARNING: All data in
    stored is deleted. """
    if(os.path.exists(database)):
        os.system('rm -rf ' + database)
        tell.info(database + ' droped!')
    else:
        tell.error(database + ' does not exists!')


def use(database):
    """ Sets the current database. """
    if(os.path.exists(database)):
        os.chdir(database)
    else:
        tell.error(database + " does not exists, or you're working in \
        the wrong collection!")


