XE-A
====

XE is the AM radio spectre designation for México, XE-A was the first radio station in México; it is also my first DBMS implementation atempt.


Development
-----------

```bash
git clone git@bitbucket.org:ecelis/xea.git
cd xea
python
```
```python
import xe
```
You'll have available:

  * xe.tell - XE Log/Debug facility
  * xe.cln - XE Database object
  * xe.db - XE database object
  * xe.t - XE table object
  * xe.dt - XE data type object


Examples
--------

```python
import xe # import XE DBMS
xe.cln.set("var") # SET COLLECTION to var
xe.db.create("db1") # SREATE DATABASE db1
xe.dt.col_type('c1', 'varchar', None, None)
```

Roadmap
-------

1. Code python methods implementing SET COLLECTION, CREATE DATABASE, 
USE DATABASE, CREATE TABLE
2. Code python methods implementing SELECT, INSERT, UPDATE, DELETE
3. Code python SQL interpreter based upon previous two points. 


*TODO*


Blueprints
----------

I wrote a [blog post](http://expressit.celisdelafuente.net/2013/09/a-learning-exercise-with-python.html)
 about the motivation when the project began

1. Features
  * Storage engine filesystem hirearchy layout
  * Stores column data with python's pickle
  * SQL operations implemented uing python's sets
  * Columns are python's dictionaries maybe lists or some mix of all of these
2. Milestones


  COLLECTION

     DATABASE
    
         TABLE = ( 
            {'column': props[type, idx, data]},
            {'column': props[type, idx, data]},
            ...
         )
        
            COLUMN

                TYPE (INT, DEC, VARCHAR, BOOLEAN)
                IDX
                DATA
hi slack
