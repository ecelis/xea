""" XE-A(lpha) main program.

Licensed to you under one or more contributor license agreements.
See the NOTICE file distributed with this work for additional
information regarding copyright ownership.  Ernesto Angel Celis de la
Fuente licenses this file to you under the Apache License, Version 2.0
(the "License"); you may not use this file except in compliance with the
License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.  """ 

import Tell as tell
import Collection as cln
import Database as db
import Table as t
import Datatype as dt

def _utest(i='cle'):
    """ A utest is a ug-dirty lil test designed to suckceed the best
    case scenario. Three tables:

    Heat(
        degrees DEC NOT NULL
    )
    Oxigen(
        description VAR NOT NULL,
        abcxyz DEC NOT NULL,
    )
    Material(
        code INT 
        description VAR,
    ) """
    tell.info('8==Starting UTestIcle==D')
    # Set collection to $XE_HOME/var TODO $XE_HOME part on stand by
    cln.create('var')
    db.create('db1')
    """t.create({'t1':
        ('c1':{NAME, DATATYPE, **}} # {'NAME':X, DATATYPE:Y, **}
        ),) """
    tell.info(' Finished --* _)\!/(_')
    """ P.S. Yes! I was on pot and thinking about sex, while typing this. """
    pass
