from setuptools import setup, find_packages
setup(
    name = "XE",
    version = "0.1",
    packages = find_packages(),
    scripts = ['Collection.py',
        'Column.py',
        'Data.py',
        'Datatype.py',
        'Logging.py',
        'Table.py',
        'Tell.py',
        'xe.py'],

    # metadata for upload to PyPI
    author = "Ernesto Celis",
    author_email = "developer@celisdelafuente.net",
    description = "Column based DB engine",
    license = "Apache 2.0",
    keywords = "db engine sql column nosql",
    url = "https://bitbucket.org/ecelis/xea",   # project home page, if any
)
