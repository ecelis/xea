""" Licensed to you under one or more contributor license agreements.
See the NOTICE file distributed with this work for additional
information regarding copyright ownership.  Ernesto Angel Celis de la
Fuente licenses this file to you under the Apache License, Version 2.0
(the "License"); you may not use this file except in compliance with the
License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License. """

import os
import shutil
import Tell

tell = Tell

def create(collection, path='.'):
    """ Creates a new collection in path, by default path = . """
    # TODO FIX Make it OS independent 
    set(path + '/' + collection, True)


def set(path='var', create=True):
    """ Sets working collection to path, if Create = True path gets
    created if it does not already exists. By default path = var and
    create is True. """
    if(os.path.exists(path)):
        os.chdir(path)
        tell.info('Collection directory set to: ' + path)
    else:
        if(create):
            # TODO FIX make this OS independent and add some error trap
            os.system('mkdir ' + path)
            tell.info(path + ' collection created.')
            os.chdir(path)
            tell.info('Collection directory set to: ' + path)
        else:
            tell.error(path + ' collection does not exists!')

