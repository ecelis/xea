""" Licensed to you under one or more contributor license agreements.
See the NOTICE file distributed with this work for additional
information regarding copyright ownership.  Ernesto Angel Celis de la
Fuente licenses this file to you under the Apache License, Version 2.0
(the "License"); you may not use this file except in compliance with the
License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License. """

import os
import Datatype as dt

class Column:
    def __init__(self):
        # SQL92 Column descriptor
        self._name = '' # SQL92 column name
        self._datatype = None
        self._value = None # SQL92 column default value TODO should be NULL in final implementation
        self._null = True # SQL92 nullability charateristic
        # TODO Do I need the column's ordinal position within the table?
        # XE specific
        self._absolutepath = ''
        self._idx = 0
        self._data = None


    def create(self, *column):
        """ Create a column, it gets a Python List as parameter which
        holds the properties for the new column.
        ['col_name', 'data_type', 'precision', 'scale']
        """
        self._name(column[0])
        if(os.path.exists(self._name)):
            tell.error(self._name + ' already existent!')
        else:
            os.system('mkdir ' + self._name)
            # TODO Verify successful directory creation before doing
            # anything else
            dt.col_type(column[0],
                    column[1],
                    column[2])
            tell.info('Column ' + column[0] + ' created!')


    def drop(column):
        """ Drops a column from database """
        if(os.path.exists(column)):
            os.system('rm -rf ' + column)
            tell.info(column + ' droped from table ') # TODO add table name


    def alter(column):
        """ Changes the table column schema """
        pass


