""" Licensed to you under one or more contributor license agreements.
See the NOTICE file distributed with this work for additional
information regarding copyright ownership.  Ernesto Angel Celis de la
Fuente licenses this file to you under the Apache License, Version 2.0
(the "License"); you may not use this file except in compliance with the
License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License. """

from __future__ import print_function
import pickle
from cStringIO import StringIO
import Tell

""" SQL defines distinct data types named by the following <key word>s:
         CHARACTER, CHARACTER VARYING, BIT, BIT VARYING, NUMERIC, DECIMAL,
         INTEGER, SMALLINT, FLOAT, REAL, DOUBLE PRECISION, DATE, TIME,
         TIMESTAMP, and INTERVAL. 

    In XE only VARCHAR, BIT, INTEGER, FLOAT and TIMESTAMP are currently
    implemented. 

    Each data type has an associated data type descriptor. The contents
         of a data type descriptor are determined by the specific data type
         that it describes. A data type descriptor includes an identifica-
         tion of the data type and all information needed to characterize an
         instance of that data type.

    An exact numeric value has a precision and a scale. The precision
         is a positive integer that determines the number of significant
         digits in a particular radix (binary or decimal). The scale is a
         non-negative integer. A scale of 0 indicates that the number is an
         integer. For a scale of S, the exact numeric value is the integer
         value of the significant digits multiplied by 10-S.

         An approximate numeric value consists of a mantissa and an expo-
         nent. The mantissa is a signed numeric value, and the exponent is
         a signed integer that specifies the magnitude of the mantissa. An
         approximate numeric value has a precision. The precision is a posi-
         tive integer that specifies the number of significant binary digits
         in the mantissa. The value of an approximate numeric value is the
         mantissa multiplied by 10exponent.

         Whenever an exact or approximate numeric value is assigned to a
         data item or parameter representing an exact numeric value, an
         approximation of its value that preserves leading significant dig-
         its after rounding or truncating is represented in the data type
         of the target. The value is converted to have the precision and
         scale of the target. The choice of whether to truncate or round is
         implementation-defined.
"""
class col_type:
    def __init__(self, col_name, d_type, d_precision, d_scale):
        if str.upper(d_type) == 'VARCHAR':
            self.sql_type = _varchar(col_name, d_precision)
        elif str.upper(d_type) == 'BIT':
            self.sql_type = _bit(col_name)
        elif str.upper(d_type) == 'FLOAT':
            self.sql_type = _float(col_name, d_precision, d_scale)
        elif str.var(d_type) == 'INTEGER':
            self.sql_type = _integer(col_name, d_precision)
        elif str.var(d_type) == 'TIMESTAMP':
            self.sql_type = _timestamp(col_name)
        else:
            self.sql_type = _null()

    def __call__(self):
        return self.sql_type


class _null:
    def __init__(self):
        self.d_type = 'NULL'

    def __str__(self):
        return 'Null'


class _varchar:
    def __init__(self, col_name, d_precision):
        self.d_type = 'VARCHAR'
        self.col_name = col_name
        self.d_precision = d_precision

class _bit:
    def __init__(self, col_name):
        self.d_type = 'BIT'
        self.col_name = col_name

class _integer:
    def __init__(self, col_name, d_precision):
        self.d_type = 'INTEGER'
        slef.col_name = col_name
        self.d_precision = d_precision


class _float:
    def __init__(self, col_name, d_precision, d_scale):
        self.d_type = 'FLOAT'
        self.con_name = col_name
        self.d_precision = d_precision
        self.d_scale = d_scale

class _timestamp:
    def __init__(self, col_name):
        self.d_type = 'TIMESTAMP'
        self.col_name = col_name

