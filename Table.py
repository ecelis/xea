""" XE Table object facilities 

Licensed to you under one or more contributor license agreements.
See the NOTICE file distributed with this work for additional
information regarding copyright ownership.  Ernesto Angel Celis de la
Fuente licenses this file to you under the Apache License, Version 2.0
(the "License"); you may not use this file except in compliance with the
License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License. """

import os
import Tell
import Collection
import Database
import Column

tell = Tell

# SQL92 Table descriptor
_name = ''
#TODO could be a txt file
_degree = None # SQL92 Table's degree (number of column descriptors)
#TODO could be the dictionary passed to create method
_column_desc = None # SQL92 column descriptor of each column 
# XE specific
_absolutepath = '' 


def create(table, columns):
    """ Creates a new table in the current database, columns is a Python
    Dictionary holding the table schema. """

    pass

def drop(table):
    """ Removes an existing table from current database. WARNING: All
    stored data will be deleted. """
    if(os.path.exists(table)):
        os.system('rm -rf ' + table)
        tell.info(table + ' droped!')


